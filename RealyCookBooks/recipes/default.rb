#
# Cookbook Name:: RealyCookBooks
# Recipe:: default
#
# Copyright (C) 2014 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

group node['realy']['group']

user node['realy']['user'] do
	group node['realy']['group'] 
	system true
	shell '/bin/bash'
end

include_recipe 'nginx::source'

